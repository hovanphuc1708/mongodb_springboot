package com.exampl.mongodb.controller;

import com.exampl.mongodb.exception.HandleExceptionMess;
import com.exampl.mongodb.service.TourService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/tour")
public class TourController {
    @Autowired
    private TourService tourService;

    @GetMapping("/get")
    public ResponseEntity<?> getTourInfos(@RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                          @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize,
                                          @RequestParam(value = "numSlot") int numSlot,
                                          @RequestParam(value = "date") String date,
                                          @RequestParam(value = "language") String language,
                                          @RequestParam(value = "currency") String currency){
        List<Document> documents = tourService.getTourInfos(pageNo, pageSize, numSlot,date, language, currency);
        Map<String, Object> map = new HashMap<>();
        map.put("status", HandleExceptionMess.okeException("Thành công"));
        map.put("data", documents);
        if (documents == null){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Dont have any tour");
        }
        return ResponseEntity.status(HttpStatus.OK).body(map);
    }

    @DeleteMapping("/delete")
    public String delete(@RequestParam(value = "id") String id) throws ExecutionException, InterruptedException {
        boolean check = tourService.checkThread(id);
        if (!check){
            return "false";
        }
        return "true";
    }
}
