package com.exampl.mongodb.controller;

import com.exampl.mongodb.entity.dto.PersonDto;
import com.exampl.mongodb.entity.subEntity.MoreInf;
import com.exampl.mongodb.service.PersonService;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/person")
public class PersonController {
    @Autowired
    private PersonService personService;

    @PostMapping("/create")
    public ResponseEntity<?> createPerson(@RequestBody PersonDto personDto) {
        try {
            Boolean checkCreate = personService.createPerson(personDto);
            if (checkCreate) {
                return ResponseEntity.status(HttpStatus.OK).body(personDto);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("ERROR: Update false");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }

    }

    @PutMapping("/update/lang/{id}")
    public ResponseEntity<?> updateLang(@PathVariable String id,@RequestParam("lang") String language) {
        try {
            if (!ObjectId.isValid(id)) {
                //Không phải là ObjectId
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Input id false");
            }
            int checkUpdate = personService.updateLangguage(id,language);

            if (checkUpdate == 2) {
                //Update thành công
                return ResponseEntity.status(HttpStatus.OK).body(language + " Added");
            }
            if (checkUpdate == 1) {
                //Language name này đã tồn tại trong person => Request không được xử lý
                return ResponseEntity.status(HttpStatus.ACCEPTED).body(language + " Already exist");
            }
            //Person id không tồn tại
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This personId: " + id + " dose not exist");

        } catch (Exception e) {
            //Một số lỗi ngoại lệ khác
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    @DeleteMapping("/delete/lang/{id}")
    public ResponseEntity<?> deleteLanguage(@PathVariable String id,@RequestParam("lang") String language) {
        try {
            if (!ObjectId.isValid(id)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Input id false");
            }
            int checkDelete = personService.deleteLanguage(id,language);

            if (checkDelete == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This personId: " + id + " dose not exist");
            }
            if (checkDelete == 2) {
                return ResponseEntity.status(HttpStatus.OK).body(language + " Deleted");
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: " + language + " does not exist");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    @PutMapping("/update/inf/{id}")
    public ResponseEntity<?> updateInf(@PathVariable String id,@RequestBody MoreInf inf) {
        try {
            if (!ObjectId.isValid(id)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Input id false");
            }
            Boolean checkIdCard = personService.checkInf(id, inf.getIdCard());
            if (checkIdCard) {
                //Check idCard tồn tại
                return ResponseEntity.status(HttpStatus.ACCEPTED).body("idCard: " + inf.getIdCard() + " Already exist");
            }
            int checkUpdate = personService.updateInfo(id,inf);
            if (checkUpdate == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This personId: " + id + " dose not exist");
            }
            return ResponseEntity.status(HttpStatus.OK).body(inf);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    @PutMapping("/update/status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable String id,@RequestParam String idCard) {
        try {
            if (!ObjectId.isValid(id)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Input id false");
            }

            int checkUpdate = personService.updateStatus(id,idCard);
            if (checkUpdate == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This personId: " + id + " dose not exist");
            }
            if (checkUpdate == 1) {
                return ResponseEntity.status(HttpStatus.ACCEPTED).body("IdCard does not exist or Status is false");
            }
            return ResponseEntity.status(HttpStatus.OK).body("Change status of: " + idCard + " to false");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    @DeleteMapping("/delete/inf/{id}")
    public ResponseEntity<?> deleteInf(@PathVariable String id,@RequestParam String idCard) {
        try {
            if (!ObjectId.isValid(id)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Input id false");
            }
            int checkdDelete = personService.deleteInf(id,idCard);
            if (checkdDelete == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This personId: " + id + " dose not exist");
            }
            if (checkdDelete == 1) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This cardId: " + idCard + " dose not exist");
            }
            return ResponseEntity.status(HttpStatus.OK).body(idCard + " Deleted");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    @PutMapping("/update/sex/{id}")
    public ResponseEntity<?> updateSex(@PathVariable String id,@RequestParam String sex) {
        try {
            if (!ObjectId.isValid(id)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Input id false");
            }
            int checkUpdate = personService.updateSex(id,sex);

            if (checkUpdate == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This personId: " + id + " dose not exist");
            }
            if (checkUpdate == 1) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("This sex: " + sex + " already exist");
            }
            return ResponseEntity.status(HttpStatus.OK).body(sex + " Updated");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }
    @PutMapping("/update/multiple/{id}")
    public ResponseEntity<?> updateMultiple(@PathVariable String id,
                                            @RequestParam("lastName") String lastName,
                                            @RequestParam("firstName") String firstName,
                                            @RequestParam("oldLang") String oldLang,
                                            @RequestParam("newLang") String newLang,
                                            @RequestBody MoreInf inf){
        try {
            if (!ObjectId.isValid(id)){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Input id false");
            }
            int checkUpdate = personService.updateMultiple(id, lastName, firstName,oldLang, newLang, inf);
            if (checkUpdate == 0){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("ERROR: This personId: " + id + " dose not exist");
            }
            if (checkUpdate == 1) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Input already exist");
            }
            return ResponseEntity.status(HttpStatus.OK).body("Updated");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    @GetMapping("/get/totalPhones")
    public ResponseEntity<?> getTotalPhones(@RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                            @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize) {

        Document doc = personService.totalPhones(pageNo,pageSize);
        if (doc == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("NO data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(doc);
    }

    @GetMapping("/get/listLang")
    public ResponseEntity<?> getListLanguage(@RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                             @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize) {

        List<Document> documents = personService.listLanguage(pageNo,pageSize);
        if (documents == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("NO data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }


    @GetMapping("/get/byParam")
    public ResponseEntity<?> personByLastNameAndBirthDay(@RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                                         @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize,
                                                         @RequestParam("name") String name,
                                                         @RequestParam("monthStart") int monthStart,
                                                         @RequestParam("monthEnd") int monthEnd) {
        //Check input
        if (!personService.checkName(name)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Name: " + name + " Does not exist");
        }
        if (monthEnd > 12 || monthEnd < 1 || monthStart > 12 || monthStart < 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: Month format: MM (MM<=12 && MM >= 1)");
        }

        List<Document> list = personService.getPersonByNameAndMonth(pageNo,pageSize,name,monthStart,monthEnd);

        if (list == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("NO data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    //TODO: get persons by sex and language name (Qes 12)
    @GetMapping("/get/byLangAndSex")
    public ResponseEntity<?> personBySexAndLanguage(@RequestParam(value = "name") String name,
                                                    @RequestParam(value = "lang") String lang,
                                                    @RequestParam(value = "sex") String sex,
                                                    @RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                                    @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize) {

        //Check name is exist
        if (!personService.checkName(name)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Name: " + name + " Does not exist");
        }
        //Check sex is exist
        if (!personService.checkSex(sex)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sex: " + sex + " Does not exist");
        }
        //Check language name is exist
        if (!personService.checkLanguage(lang)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Language name : " + lang + " Does not exist");
        }
        List<Document> documents = personService.personBySexAndLanguage(lang,sex,pageNo,pageSize);
        if (documents == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("No data");
        }
        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }

    //TODO: Count persons by sex and language name (Qes 13)
    @GetMapping("/get/countByInput")
    public ResponseEntity<?> countBySexAndLanguage(@RequestParam(value = "lang") String lang,
                                                    @RequestParam(value = "sex") String sex,
                                                    @RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                                    @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize){
         //Check sex is exist
        if (!personService.checkSex(sex)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sex: " + sex + " Does not exist");
        }
        //Check language name is exist
        if (!personService.checkLanguage(lang)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Language name : " + lang + " Does not exist");
        }
        List<Document> documents = personService.countFirstAndLast(lang, sex, pageNo, pageSize);
        if (documents == null){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("No data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }

}