package com.exampl.mongodb.controller;

import com.exampl.mongodb.entity.Employee;
import com.exampl.mongodb.entity.dto.EmployeeDto;
import com.exampl.mongodb.service.EmployeeService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    //1
    @GetMapping("/get/each")
    public ResponseEntity<?> totalCompanyAndEmployeeOfEach(){
        Document document = employeeService.companyAndEmployeeOfEach();
        if (document == null){
            return ResponseEntity.status(HttpStatus.RESET_CONTENT).body("No data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(document);
    }

    //2
    @GetMapping("/get/salary/year")
    public ResponseEntity<?> getSalaryBy(@RequestParam("id") String id,
                                         @RequestParam("year") int year,
                                         @RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                         @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize){
        List<Document> documents = employeeService.salaryByYear(id, year, pageNo, pageSize);
        if (documents == null){
            return ResponseEntity.status(HttpStatus.RESET_CONTENT).body("No data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }

    //3
    @GetMapping("/get/salary")
    public ResponseEntity<?> getSalary(@RequestParam("year") int year,
                                       @RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                       @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize){
        List<Document> documents = employeeService.salaryOfCompany(year, pageNo, pageSize);

        if (documents == null){
            return ResponseEntity.status(HttpStatus.RESET_CONTENT).body("No data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }

    //4
    @GetMapping("/get/salary/active")
    public ResponseEntity<?> getSalaryByActive(@RequestParam("categories") String categories,
                                               @RequestParam("yearStart") int yearStart,
                                               @RequestParam("yearEnd") int yearEnd,
                                               @RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                               @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize){
        List<Document> documents = employeeService.salaryByActive(categories, yearStart, yearEnd, pageNo, pageSize);

        if (documents == null){
            return ResponseEntity.status(HttpStatus.RESET_CONTENT).body("No data return");
        }
        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createEmployee(@RequestBody EmployeeDto employeeDto){
        try {
            int checkInsert = employeeService.checkInsert(employeeDto);
            switch (checkInsert) {
                case 0 -> {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Company or Person does not exist");
                }
                case 2 -> {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Information of employee is not valid");
                }
                case 3 -> {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Company is not valid");
                }
                case 4 -> {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Currency of employee is not valid with company");
                }
            }
            EmployeeDto emp = employeeService.insertEmployee(employeeDto);
            if (emp == null){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("ERROR: System");
            }
            return ResponseEntity.status(HttpStatus.OK).body(emp);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    @GetMapping("/person/{id}")
    public ResponseEntity<?> getPersonInformation(@PathVariable String id,
                                                  @RequestParam("language") String language,
                                                  @RequestParam("currency") String currency,
                                                  @RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
                                                  @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize){
        Boolean check = employeeService.checkCompany(id);
        if (!check){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Company ID: "+id+" does not exist");
        }

        List<Document> documents = employeeService.getPersonInf(id, language, currency, pageNo, pageSize);
        if (documents == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Company id: "+id+ " don't have any employee");
        }
        return ResponseEntity.status(HttpStatus.OK).body(documents);
    }

    //Get employees and convert salary to currencyInput
    @GetMapping("/gets")
    public ResponseEntity<?> getAll(
    		@RequestParam(value = "pageNo", defaultValue = "1", required = false) int pageNo,
            @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize){
    	List<Employee> employees = employeeService.getAll(pageSize, pageNo);
    	
    	return ResponseEntity.status(HttpStatus.OK).body(employees);
    
    }



}
