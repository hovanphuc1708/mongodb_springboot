package com.exampl.mongodb.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.exampl.mongodb.entity.Exchange;
import com.exampl.mongodb.entity.Tour;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

@Repository
public class ExchangeRepository {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
    private MongoDatabase mongoDatabase;

    private MongoCollection<Exchange> mongoCollection;
	
    @Autowired
    public void ExchangeRepo() {
        mongoCollection = mongoDatabase.getCollection("exchange", Exchange.class);
    }
    
    public Exchange findByCurrency(String currency){
    	Query query = new Query().addCriteria(Criteria.where("currency").is(currency));
    	return mongoTemplate.findOne(query, Exchange.class);
    }
	
}
