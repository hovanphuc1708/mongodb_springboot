package com.exampl.mongodb.repository;

import com.exampl.mongodb.entity.Company;
import com.exampl.mongodb.entity.Person;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class CompanyRepository {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private MongoDatabase mongoDatabase;

    private MongoCollection<Company> mongoCollection;

    @Autowired
    public void CompanyRepo() {
        mongoCollection = mongoDatabase.getCollection("company", Company.class);
    }

    public Document companyAndEmployeeOfEach(){
        List<Bson> pipeline = new ArrayList<>();
        Bson facet = new Document("$facet",
                new Document("countCompany", Arrays.asList(new Document("$count", "totalCompany"))).
                        append("EmpOfCompany", Arrays.asList(new Document("$project", new Document("_id", 0).
                                append("names", 1).
                                append("employeeNumb", 1)))));
        pipeline.add(facet);

        return mongoCollection.aggregate(pipeline, Document.class).first();
    }
    public List<Company> getCompanyByActive(String categories){
        Query query = new Query();
        query.addCriteria(Criteria.where("categories").in(categories));
        return mongoTemplate.find(query, Company.class);
    }

    public Company getCompanyById(ObjectId id){
        Query query = new Query().addCriteria(Criteria.where("_id").is(id));
        return mongoTemplate.findOne(query, Company.class);
    }

    public Document getCompanyName(String companyId, String language){
        List<Bson> pipeline = new ArrayList<>();
        Bson match = new Document("$match", new Document("_id", new ObjectId(companyId)));

        Bson project = new Document("$project", new Document("_id", 0)
                .append("names",
                        new Document("$cond",
                                Arrays.asList(new Document("$in", Arrays.asList(language, "$names.lang")),
                                        new Document("$filter",
                                                new Document("input", "$names").append("as", "names").append("cond",
                                                        new Document("$eq", Arrays.asList("$$names.lang", language)))),
                                        new Document("$first", "$names")))));
        pipeline.add(match);
        pipeline.add(project);
        return mongoCollection.aggregate(pipeline, Document.class).first();
    }
}
