package com.exampl.mongodb.repository;

import com.exampl.mongodb.entity.DateOpen;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DateOpenRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoDatabase mongoDatabase;

    private MongoCollection<DateOpen> mongoCollection;

    @Autowired
    public void TourPrice() {
        mongoCollection = mongoDatabase.getCollection("dateOpen", DateOpen.class);
    }

    public List<DateOpen> getOpenTours(String date,List<ObjectId> list){
        Query query = new Query();
        query.addCriteria(Criteria.where("dateAvailable").is(date));
        query.addCriteria(Criteria.where("status").is(true));
        query.addCriteria(Criteria.where("tourId").in(list));

        return mongoTemplate.find(query, DateOpen.class);
    }
}
