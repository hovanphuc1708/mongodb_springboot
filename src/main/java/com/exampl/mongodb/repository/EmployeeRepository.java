package com.exampl.mongodb.repository;

import com.exampl.mongodb.entity.Employee;
import com.exampl.mongodb.entity.dto.EmployeeDto;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.result.InsertOneResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class EmployeeRepository {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoDatabase mongoDatabase;
    

    private MongoCollection<Employee> mongoCollection;
    @Autowired
    public void EmployeeRepo() {
        mongoCollection = mongoDatabase.getCollection("employee", Employee.class);
    }

    public List<Document> salaryByYear(String id,int year,int pageNo,int pageSize){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        //match:
        //1. year = year
        //2. companyId = id
        List<Document> filters = new ArrayList<>();
        Document compIdFilter = new Document("companyId", new ObjectId(id));
        Document yearFilter = new Document("$expr", new Document("$eq",  Arrays.asList(new Document("$year", "$workStart"), year)));
        filters.add(compIdFilter);
        filters.add(yearFilter);
//        Bson match = new Document("$match", new Document("$and",Arrays.asList(
//                new Document("$expr", new Document("$eq",  Arrays.asList(new Document("$year", "$workStart"), year))),
//                compIdFilter)));
        Bson match =  new Document("$match", new Document("$and", filters));

        //group:
        //1. group by companyId
        //2. totalSalary = sum của salary
        //3  totalEmployee = sum của group by companyId
        Bson group = new Document("$group", new Document("_id", "$companyId").
                append("totalSalary", new Document("$sum", "$salary")).
                append("totalEmployee", new Document("$sum", 1)));
        Bson project = new Document("$project", new Document("_id", new Document("$toString", "$_id"))
                .append("totalSalary", 1)
                .append("totalEmployee", 1)
        );

        pipeline.add(match);
        pipeline.add(group);
        pipeline.add(project);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> documentList = new ArrayList<>();
        mongoCollection.aggregate(pipeline, Document.class).into(documentList);

        return documentList;
    }

    public List<Document> salaryOfCompany(int year, int pageNo,int pageSize){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        Bson match = new Document("$match", new Document("$expr", new Document("$eq", Arrays.asList(new Document("$year", "$workStart"), year))));

        //group:
        //1. group by companyId
        //2. totalSalary = sum của salary
        //3. totalEmployee = sum của group by companyId
        Bson group = new Document("$group", new Document("_id", "$companyId").
                append("totalSalary", new Document("$sum", "$salary")).
                append("totalEmployee", new Document("$sum", 1)));

        pipeline.add(match);
        pipeline.add(group);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> documentList = new ArrayList<>();
        mongoCollection.aggregate(pipeline, Document.class).into(documentList);

        return documentList;
    }

    public List<Document> salaryByActive(int pageSize, int pageNo, int yearStart, int yearEnd, List<ObjectId> listId){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        //addFields
        Bson addField = new Document("$addFields", new Document("year", new Document("$year", "$workStart")));
        //match and:
        //1. match year lớn hơn yearStart
        //2. match year nhỏ hơn yearEnd
        //3. match in list Object Id của company

        Bson match = new Document("$match", new Document("$and", Arrays.asList(
                new Document("$expr", new Document("$gte",  Arrays.asList(new Document("$year", "$workStart"), yearStart))),
                new Document("$expr", new Document("$lte",  Arrays.asList(new Document("$year", "$workStart"), yearEnd))),
                new Document("companyId", new Document("$in", listId))
        )));

        //group:
        //1. group by  year + companyId
        //2. totalSalary = sum của salary
        //3. totalEmployee = sum của group by year + companyId
        Bson group = new Document("$group",
                new Document("_id", new Document("companyId", "$companyId").
                        append("date", "$year")).
                        append("totalSalary", new Document("$sum", "$salary")).
                        append("totalEmployee", new Document("$sum", 1)));
        Bson project = new Document("$project", new Document("companyId", new Document("$toString", "$companyId"))
                .append("year", 1)
                .append("salary", 1)

        );


        pipeline.add(addField);
        pipeline.add(match);
        pipeline.add(project);
        pipeline.add(group);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> documentList = new ArrayList<>();
        mongoCollection.aggregate(pipeline, Document.class).into(documentList);

        return documentList;
    }

    public InsertOneResult insertEmployee(Employee employee){
        return mongoCollection.insertOne(employee);
    }

    public List<Document> getEmployees(String companyId, int pageNo,int pageSize){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        Bson match = new Document("$match", new Document("companyId", new ObjectId(companyId)));
        pipeline.add(match);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> docs = new ArrayList<>(); //List Employee

        return mongoCollection.aggregate(pipeline, Document.class).into(docs);
    }

    public List<Employee> getAll(int pageSize, int pageNo){
    	Query query = new Query();
    	query.skip((pageNo - 1) * pageSize);
    	query.limit(pageSize);
    
    	return mongoTemplate.find(query, Employee.class);
    }
}
