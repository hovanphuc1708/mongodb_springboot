package com.exampl.mongodb.repository;

import com.exampl.mongodb.entity.Tour;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TestRepo {
    @Autowired
    private MongoDatabase database;

    private MongoCollection<Document> mongoCollection;

    @Autowired
    public void TourService() {
        mongoCollection = database.getCollection("t600", Document.class);
        getT600();
    }

    private void getT600(){
        List<Bson>  pipeline = new ArrayList<>();
        Integer status = 1;
        Document match = new Document("$match", new Document("tn601", status - 1));

        pipeline.add(match);

        List<Document> docs = new ArrayList<>();
        mongoCollection.aggregate(pipeline, Document.class).into(docs);
        System.out.println("RETURN "+docs);
    }
}
