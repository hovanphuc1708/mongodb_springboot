package com.exampl.mongodb.repository;

import com.exampl.mongodb.entity.PriceTour;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.bson.Document;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class TourPriceRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private MongoDatabase mongoDatabase;

    private MongoCollection<PriceTour> mongoCollection;

    @Autowired
    public void PriceTourService() {
        mongoCollection = mongoDatabase.getCollection("tourPrice", PriceTour.class);
    }

    public List<Document> getTourPrices(String date,List<ObjectId> listId, int numSlot){
        List<Bson> pipeline = new ArrayList<>();
        Bson match = new Document("$match", new Document("$and", Arrays.asList(
                new Document("dateApplyStart", new Document("$lte", LocalDate.parse(date))),
                new Document("dateApplyEnd", new Document("$gte", LocalDate.parse(date))),
                new Document("tourId", new Document("$in", listId))
        )));
        Bson project = new Document("$project", new Document("total", new Document("$multiply", Arrays.asList("$price", numSlot)))
                //Output numSlot, price, currency để tính totalPrice ở step này và add tour ở step 4
                .append("tourId", 1)
                .append("numSlot", numSlot)
                .append("price", 1)
                .append("currency", 1)
        );
        Bson sort1 = new Document("$sort", new Document("tourId", 1));
        pipeline.add(match);
        pipeline.add(project);
        pipeline.add(sort1);
        List<Document> tourPrice = new ArrayList<>();
        mongoCollection.aggregate(pipeline, Document.class).into(tourPrice);
        return tourPrice;
    }


}
