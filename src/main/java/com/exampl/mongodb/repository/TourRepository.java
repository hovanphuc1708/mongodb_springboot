package com.exampl.mongodb.repository;

import com.exampl.mongodb.entity.Tour;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Repository
public class TourRepository{
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private MongoDatabase mongoDatabase;

    private MongoCollection<Tour> mongoCollection;

    @Autowired
    public void TourService() {
        mongoCollection = mongoDatabase.getCollection("tour",Tour.class);
    }

    public List<Document> getTours(int pageSize, int pageNo, String language, int numbSot){
        List<Bson> pipeline = new ArrayList<>();
        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);
        Bson match = new Document("$match", new Document("slot", new Document("$gte", numbSot)));
        Bson project = new Document("$project", new Document("slot",1)
                .append("_id", new Document("$toString", "$_id"))
                .append("infos",
                        new Document("$cond",
                                Arrays.asList(new Document("$in", Arrays.asList(language, "$infos.language")),
                                        new Document("$filter",
                                                new Document("input", "$infos").append("as", "infosMap").append("cond",
                                                        new Document("$eq", Arrays.asList("$$infosMap.language", language)))),
                                        new Document("$first", "$infos")))));
        Bson sort = new Document("$sort", new Document("tourId", 1));

        pipeline.add(match);
        pipeline.add(project);
        pipeline.add(skip);
        pipeline.add(limit);
        pipeline.add(sort);
        List<Document> tours = new ArrayList<>();
        mongoCollection.aggregate(pipeline, Document.class).into(tours);

        return tours;
    }

    public Tour findTourBySlot(int numSlot){
        Query query = new Query().addCriteria(Criteria.where("slot").gte(numSlot));
        return mongoTemplate.findOne(query, Tour.class);
    }


    public int demoMultipleThread(String id)throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        Bson filter = Filters.eq("_id",new ObjectId(id));

        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(
                () -> {
                    DeleteResult rs = mongoCollection.deleteOne(filter);
                    DeleteResult rs1 = mongoCollection.deleteOne(filter);
                    if (rs.getDeletedCount() == rs1.getDeletedCount()){
                        return 1;
                    }else {
                        return 2;
                    }
                });
       if (future1.get() == 1){
           return 1;
       }
       return  2;
    }

}
