package com.exampl.mongodb.repository;

import com.exampl.mongodb.entity.Person;
import com.exampl.mongodb.entity.dto.PersonDto;
import com.exampl.mongodb.entity.subEntity.MoreInf;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Repository
public class PersonRepository {
    @Autowired
    private MongoDatabase mongoDatabase;
    @Autowired
    private MongoTemplate mongoTemplate;

    private MongoCollection<Person> mongoCollection;

    @Autowired
    public void PersonRepo() {
        mongoCollection = mongoDatabase.getCollection("person", Person.class);
    }

    public InsertOneResult insertOne(Person person) {
        return mongoCollection.insertOne(person);
    }
    //UpdateLanguage
    public  UpdateResult updateLanguage(String id, String language) {
        Bson filter = Filters.eq("_id",new ObjectId(id));

        Bson update = Updates.addToSet("langs",language);

        return mongoCollection.updateOne(filter,update);
    }
    //Find a person by Id and idCard
    public Person findPerson(String id, String idCard){
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(new ObjectId(id)));
        query.addCriteria(Criteria.where("infors.idCard").is(idCard));
        return mongoTemplate.findOne(query, Person.class);
    }

    //Find person by fullName
    public List<Document> findByFullName(String name){
        List<Bson> pipeline = new ArrayList<>();

        Bson project = new Document("$project", new Document("fullName", new Document("$concat", Arrays.asList("$firstName", " ", "$lastName"))));
        Bson match  = new Document("$match", new Document("fullName", new Document("$regex",  Pattern.compile(name+"(?i)"))));

        pipeline.add(project);
        pipeline.add(match);

        List<Document> documents = new ArrayList<>();
        documents = mongoCollection.aggregate(pipeline, Document.class).into(documents);
        return documents;
    }

    //Find a person by language
    public Person findByLanguage(String language){
        Query query = new Query().addCriteria(Criteria.where("langs").is(language));
        return mongoTemplate.findOne(query, Person.class);
    }
    //Find a person by sex
    public Person findBySex(String sex){
        Query query = new Query().addCriteria(Criteria.where("sex").is(sex));
        return mongoTemplate.findOne(query, Person.class);
    }

    //Find a person by Id
    public Person getPersonById(ObjectId id){
        Query query = new Query().addCriteria(Criteria.where("_id").is(id));
        return mongoTemplate.findOne(query, Person.class);
    }

    //Delete a language in a person
    public UpdateResult deleteLanguage(String id, String language){
        Bson filter = Filters.eq("_id", new ObjectId(id));
        Bson update = Updates.pull("langs", language);
        return mongoCollection.updateOne(filter, update);
    }

    //Update a inf in a person
    public UpdateResult updateInf(String id,MoreInf inf){
        Bson filter = Filters.eq("_id", new ObjectId(id));

        Bson update = Updates.push("infors", inf);
        return mongoCollection.updateOne(filter, update);
    }
    //Update status of inf to false
    public UpdateResult updateStatusOfInf(String id, String idCard){
        Bson filter = Filters.eq("_id", new ObjectId(id));
        Bson update = Updates.set("infors.$[x].status", false);
        UpdateOptions op = new UpdateOptions().
                arrayFilters(Arrays.asList(new Document("x.idCard", idCard)));

        return mongoCollection.updateOne(filter, update, op);
    }
    //delete inf in a person
    public UpdateResult deleteInf(String id, String idCard){
        Bson filter = Filters.eq("_id", new ObjectId(id));
        Bson update = Updates.pull("infors", new Document("idCard", idCard));

        return mongoCollection.updateOne(filter, update);
    }

    //Update firstName, lastName, Language, Inf
    public UpdateResult updateMultiple(String id, String lastName, String firstName,String oldLang, String newLang, MoreInf inf){
        Bson filter = Filters.eq("_id", new ObjectId(id));
        Bson newValue = new Document("lastName", lastName).
                append("firstName", firstName).
                append("infors.$[x].address", inf.getAddress()).
                append("infors.$[x].status", inf.getStatus()).
                append("langs.$[y]", newLang);

        Bson updates = new Document("$set", newValue);

        UpdateOptions op = new UpdateOptions().
                arrayFilters(Arrays.asList(new Document("x.idCard", inf.getIdCard()),
                        new Document("y", oldLang)));

        return mongoCollection.updateMany(filter, updates, op);
    }

    //Update sex in a person
    public UpdateResult updateSex(String id, String sex){
        Bson filter = Filters.eq("_id", new ObjectId(id));
        Bson update = Updates.set("sex", sex);

        return mongoCollection.updateOne(filter, update);
    }

    //Count phones in collection
    public Document countPhone(int pageNo,int pageSize){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        Bson unwind = new Document("$unwind", "$phone");
        Bson group = new Document("$group", new Document("_id", "$phone"));
        Bson count = new Document("$count", "totalPhones");

        pipeline.add(unwind);
        pipeline.add(group);
        pipeline.add(count);
        pipeline.add(skip);
        pipeline.add(limit);

        return mongoCollection.aggregate(pipeline,Document.class).first();
    }

    //Get languages in collection
    public List<Document> getLanguages(int pageNo,int pageSize){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        Bson unwind = new Document("$unwind", "$langs");
        Bson group = new Document("$group", new Document("_id", "$langs"));

        pipeline.add(unwind);
        pipeline.add(group);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> documents = new ArrayList<>();
        documents = mongoCollection.aggregate(pipeline, Document.class).into(documents);
        return documents;
    }

    //Find persons by fullName and birthDay
    public List<Document> findByNameAndBirthDay(int pageNo,int pageSize, String name, int monthStart, int monthEnd){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        Bson project = new Document("$project", new Document("_id", 0).
                append("month", new Document("$month", "$birthDay")).
                append("fullName", new Document("$concat", Arrays.asList("$lastName", " ", "$firstName"))));

        Bson match = new Document("$match", new Document("$and", Arrays.asList(
                new Document("$and", Arrays.asList(
                        new Document("month", new Document("$gte", monthStart)).
                                append("month", new Document("$lte", monthEnd))
                )).
                        append("fullName", new Document("$regex", Pattern.compile(name + "(?i)")))
        )));

        pipeline.add(project);
        pipeline.add(match);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> documents = new ArrayList<>();
        documents = mongoCollection.aggregate(pipeline, Document.class).into(documents);
        return documents;
    }

    //Find persons by name and language
    public List<Document> findByNameAndLanguage(String langName,String sex,int pageNo,int pageSize){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        Bson match = new Document("$match",new Document("$and", Arrays.asList(
                new Document("sex", sex).append("langs", langName)
        )));

        Bson project = new Document("$project",
                new Document("fullName",new Document("$concat",Arrays.asList("$firstName"," ","$lastName")))
                        .append("phone",1)
                        .append("_id",0)
                        .append("lang",
                                new Document("$filter",
                                        new Document("input","$langs").append("as","lang").append("cond",
                                                new Document("$eq",Arrays.asList("$$lang",langName)))))
                        .append("email",
                                new Document("$filter",
                                        new Document("input","$emails").append("as","email").append("cond",
                                                new Document("$regexMatch",new Document("input","$$email.email")
                                                        .append("regex",Pattern.compile("@gmail.com(?i)"))))))
                        .append("sex", 1));

        pipeline.add(match);
        pipeline.add(project);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> documents = new ArrayList<>();
        documents = mongoCollection.aggregate(pipeline,Document.class).into(documents);
        return documents;
    }

    //Count Document before and after query => use facet
    public List<Document> countFirstAndLast(String langName, String sex, int pageNo, int pageSize){
        List<Bson> pipeline = new ArrayList<>();

        Bson limit = Aggregates.limit(pageSize);
        Bson skip = Aggregates.skip((pageNo - 1) * pageSize);

        Bson facet = new Document("$facet",
                //Count first to filter
                new Document("countFirst",Arrays.asList(new Document("$count","countFirst"))).
                        //Count last
                                append("countLast",Arrays.asList(
                                new Document("$match",new Document("$and", Arrays.asList(
                                        new Document("sex", sex).append("langs", langName)
                                ))),
                                new Document("$count","countLast")))
        );
        Bson project = new Document("$project",new Document("countFirst",new Document("$arrayElemAt",Arrays.asList("$countFirst.countFirst",0))).
                append("countLast",new Document("$arrayElemAt",Arrays.asList("$countLast.countLast",0))));

        pipeline.add(facet);
        pipeline.add(project);
        pipeline.add(skip);
        pipeline.add(limit);

        List<Document> documents = new ArrayList<>();
        mongoCollection.aggregate(pipeline,Document.class).into(documents);
        return documents;
    }

    //Get persons by List Object Id
    public List<Document> geListPerson(List<ObjectId> idList){
        List<Bson> pipeline = new ArrayList<>();

        Bson match = new Document("$match", new Document("_id", new Document("$in", idList)));
        Bson project = new Document("$project", new Document("fullName", new Document("$concat", Arrays.asList("$lastName", " ", "$firstName")))
                .append("_id", new Document("$toString", "$_id"))
                .append("phone", 1)
                .append("emails", 1)
                .append("birthDay", 1)
                .append("age", 1)
                .append("sex", 1)
                .append("infors", 1)
                .append("langs", 1)
        );
        pipeline.add(match);
        pipeline.add(project);
        List<Document> documents = new ArrayList<>();
        mongoCollection.aggregate(pipeline, Document.class).into(documents);
        return documents;
    }


}
