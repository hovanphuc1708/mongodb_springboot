package com.exampl.mongodb.service;

import com.exampl.mongodb.entity.Person;
import com.exampl.mongodb.entity.dto.PersonDto;
import com.exampl.mongodb.entity.subEntity.MoreInf;
import com.exampl.mongodb.repository.PersonRepository;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepo;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MongoDatabase mongoDatabase;
    private MongoCollection<Person> mongoCollection;
    @Autowired
    public void PersonServiceImpl() {
        mongoCollection = mongoDatabase.getCollection("person", Person.class);
    }

    // 1. Viết query insert đầy đủ data của 1 document trong collection person
    public Boolean createPerson(PersonDto personDto) {
        Person person = modelMapper.map(personDto, Person.class);
        InsertOneResult result = personRepo.insertOne(person);

        if (result.wasAcknowledged()) {
            return true;
        }
        return false;
        
    }

    // 2. Viết query update thêm 1 language của 1 person
    public  int updateLangguage(String id, String language){
        UpdateResult result = personRepo.updateLanguage(id, language);

        //Person id does not exist
        if (result.getMatchedCount() == 0){
            return 0;
        }
        //Language name already exist
        if (result.getMatchedCount() == 1 && result.getModifiedCount() == 0){
            return 1;
        }
        //Add language successful
        return 2;
    }

    //Check idCard in person is exist
    public Boolean checkInf(String id, String idCard){
        Person person = personRepo.findPerson(id, idCard);
        if (person == null){
            return false;
        }
        return true;
    }

    //Check name in person
    public Boolean checkName(String name){
        List<Document> documents = personRepo.findByFullName(name);
        if (documents.isEmpty()){
            return false;
        }
        return true;
    }

    //Check language in person
    public Boolean checkLanguage(String langName){
        Person person = personRepo.findByLanguage(langName);
        if (person == null){
            return false;
        }
        return true;
    }

    //Check language in person
    public Boolean checkSex(String sex){
        Person person = personRepo.findBySex(sex);
        if (person == null){
            return false;
        }
        return true;
    }


    // 3. Viết query xoá 1 language của 1 person
    public int deleteLanguage(String id, String name){
        UpdateResult result = personRepo.deleteLanguage(id, name);

        //Id input không tồn tại
        if (result.getMatchedCount() == 0){
            return 0;
        }
        //Language name không tồn tại
        if (result.getMatchedCount() == 1 && result.getModifiedCount() == 0){
            return 1;
        }
        //Update thành công
        return 2;
    }

    // 4. Viết query update thêm 1 info của 1 person
    public int updateInfo(String id,MoreInf inf){
        UpdateResult result = personRepo.updateInf(id, inf);
        Map<String, MoreInf> map = new HashMap<>();
        if (result.getMatchedCount() == 0){
            //thất bại
            return 0;
        }
        //thành công
        return 1;
    }

    // 5. Viết query update infors của 1 user thành deactive (ko còn sử dụng nữa) => status => false
    public int updateStatus(String id, String idCard){
        UpdateResult result = personRepo.updateStatusOfInf(id, idCard);

        if (result.getMatchedCount() == 0){
            return 0;
        }
        //idCard không tồn tại hoặc infors.status == false
        if (result.getMatchedCount() == 1 && result.getModifiedCount() == 0){
            return 1;
        }
        return 2;
    }

    // 6. Viết query xoá 1 info của 1 person
    public int deleteInf(String id, String idCard){
        UpdateResult result = personRepo.deleteInf(id, idCard);

        if (result.getMatchedCount() == 0){
            return 0;
        }
        //idCard does not exist
        if (result.getMatchedCount() == 1 && result.getModifiedCount() == 0){
            return 1;
        }
        return 2;
    }

    // 7. Viết query update họ/tên/info của 1 person
    public int updateMultiple(String id, String lastName, String firstName,String oldLang, String newLang, MoreInf inf){
        UpdateResult result =personRepo.updateMultiple(id, lastName, firstName, oldLang, newLang, inf);

        if (result.getMatchedCount() == 0){
            return 0;
        }
        if (result.getMatchedCount() == 1 && result.getModifiedCount() == 0){
            return 1;
        }

        return 2;

    }
    // 8. Viết query cập nhật giới tính của toàn bộ document trong collection person sang giới tính khác
    public int updateSex(String id, String sex){
        UpdateResult result = personRepo.updateSex(id, sex);

        if (result.getMatchedCount() == 0){
            return 0;
        }
        if(result.getMatchedCount() == 1 && result.getModifiedCount() == 0){
            return 1;
        }
        return 2;
    }

    // 9. Viết query đếm trong collection person có bao  nhiêu sdt
       //Output: Total phones in collection(không trùng nhau)
    public Document totalPhones(int pageNo,int pageSize){
        Document doc = personRepo.countPhone(pageNo, pageSize);
        if (!doc.isEmpty()){
            return doc;
        }
        return null;
    }

    // 10. Viết query get toàn bộ language hiện có trong collection person (kết quả ko được trùng nhau)
        //Output: list languages
    public List<Document> listLanguage(int pageNo,int pageSize){
        List<Document> documents = personRepo.getLanguages(pageNo, pageSize);
        if (documents.isEmpty()){
            return null;
        }
        return documents;
    }

    // 11. Viết query get những person có tên chứa "Nguyễn" và ngày sinh trong khoảng tháng 2~ tháng 10
        //Input: name, tháng sinh (monthStart -> monthEnd)
        //Output: List person
    public List<Document> getPersonByNameAndMonth(int pageNo,int pageSize, String name, int monthStart, int monthEnd){
        List<Document> documents = personRepo.findByNameAndBirthDay(pageNo, pageSize, name, monthStart, monthEnd);
        if (documents.isEmpty()){
            return null;
        }
        return documents;
    }

    // 12. Viết query get thông tin của toàn bộ person có giới tính là nam + language là "Tiếng Việt", yêu cầu:
    //     - Group theo fullname (họ + tên)
    //     - Kết quả trả về bao gồm:
    //         + fullname (họ + tên)
    //         + sdt
    //         + language (chỉ hiển thị language "Tiếng Việt")
    //         + email (chỉ hiển thị những email có đuôi là @gmail.com)
       //Input: giới tính(sex), language
       //Output: List person info theo yêu cầu
    public List<Document> personBySexAndLanguage(String langName,String sex,int pageNo,int pageSize) {
        List<Document> documents = personRepo.findByNameAndLanguage(langName, sex, pageNo, pageSize);

        if (documents.isEmpty()) {
            return null;
        }
        return documents;
    }

       //Input: language, sex
       //Output:
            //- countFirst: số document khi chưa match
            //- countLast: số document sau khi match
    public List<Document> countFirstAndLast(String langName, String sex, int pageNo, int pageSize) {
        List<Document> documents = personRepo.countFirstAndLast(langName, sex, pageNo, pageSize);
        if (documents.isEmpty()){
            return null;
        }
        return documents;
    }

}




