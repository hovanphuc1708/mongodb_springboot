package com.exampl.mongodb.service;

import com.exampl.mongodb.entity.Company;
import com.exampl.mongodb.entity.Employee;
import com.exampl.mongodb.entity.Exchange;
import com.exampl.mongodb.entity.Person;
import com.exampl.mongodb.entity.dto.EmployeeDto;
import com.exampl.mongodb.entity.subEntity.Certificate;
import com.exampl.mongodb.entity.subEntity.Verhicle;
import com.exampl.mongodb.repository.CompanyRepository;
import com.exampl.mongodb.repository.EmployeeRepository;
import com.exampl.mongodb.repository.PersonRepository;
import com.mongodb.client.result.InsertOneResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class EmployeeService {
    @Autowired
    private CompanyRepository companyRepo;
    @Autowired
    private EmployeeRepository employeeRepo;
    @Autowired
    private PersonRepository personRepo;
    @Autowired
    private ModelMapper modelMapper;



//1 Tổng số lượng công ty và số lượng nhân viên tối đa của mỗi công ty
    //Output 1: tổng số công ty
    //Output 2:
    //Tên công ty
    //"maxEmployee" của công ty
    public Document companyAndEmployeeOfEach(){
        //Note: Convert format
        Document doc = companyRepo.companyAndEmployeeOfEach();
        //=>
        if (doc.isEmpty()){
            return null;
        }
        return doc;
    }

    //2.  Thống kê công ty A , vào năm 2022 có bao nhiêu nhân viên vào làm, tổng mức lương phải trả cho những nhân viên đó là bao nhiêu
    //Match đến Id company có tên A + employee có "workStart" là 2022
    //Output:
    //Id công ty A
    //Tổng lương phải trả
    //Số lượng nhân viên trong công ty
    public List<Document> salaryByYear(String id, int year, int pageNo,int pageSize){
        List<Document> documentList = employeeRepo.salaryByYear(id, year, pageNo, pageSize);
        if (documentList.isEmpty()){
            return null;
        }
        return documentList;
    }

    //3. Thống kê tổng số tiền các công ty phải trả cho những người đăng ký vào làm trong năm 2022
    //Match đến nhưng employee có "workStart" year 2022
    //Output:
    //Id của mỗi công ty
    //Tổng lương phải trả của mỗi công ty
    //Tổng số nhân viên của mỗi công ty
    public List<Document> salaryOfCompany(int year, int pageNo,int pageSize){
        List<Document> documentList = employeeRepo.salaryOfCompany(year, pageNo, pageSize);
        if (documentList.isEmpty()) {
            return null;
        }
        return documentList;

    }

    //4. Thống kê tổng số tiền các công ty IT phải trả cho những người đăng ký vào làm trong các năm từ 2020 ~ 2022
    //Match đến các Id company có "categories" là IT và những employee có "workStart" từ yearStart -> yearEnd
    //Output theo từng năm, trong mỗi năm có:
    //1. Id của công ty + năm
    //2. Tổng lương phải trả của công ty trong năm đó
    //3. Số lương nhân viên của công ty trong năm đó
    public List<Document> salaryByActive(String categories, int yearStart, int yearEnd, int pageNo,int pageSize) {
        //Get list ObjectId của company có active là IT
        List<ObjectId> listId = new ArrayList<>();
        List<Company> companies = companyRepo.getCompanyByActive(categories);
        for (Company company : companies){
            listId.add(company.getId());
        }

        List<Document> documentList = employeeRepo.salaryByActive(pageSize, pageNo, yearStart, yearEnd, listId);
        if (documentList.isEmpty()) {
            return null;
        }
        return documentList;
    }

    //Check company is exist
    public Boolean checkCompany(String id){
        Company company =companyRepo.getCompanyById(new ObjectId(id));
        if (company == null){
            return false;
        }
        return true;
    }


//    1.API đăng ký 1 nhân viên vào làm cho 1 company, yêu cầu:
//      Check thông tin nhân viên hợp lệ (nhân viên có bằng cấp tương ứng với company, có sử dụng ít nhất 1 ngôn ngữ, có phương tiện sử dụng)
//      Check Company hợp lệ (có đơn vị tiền tệ trả lương, đang hoạt động)
//      Check currency trả lương phải đúng với currency của company
    public EmployeeDto insertEmployee(EmployeeDto employeeDto){
        Employee employee = modelMapper.map(employeeDto, Employee.class);
        //Insert employee
        InsertOneResult result = employeeRepo.insertEmployee(employee);
        if (result.wasAcknowledged()){
            //insert thành công
            return employeeDto;
        }
        //thất bại
        return null;
    }
    //Kiểm tra person Certificates có phù hợp với company
    private Boolean checkCategory(Person person,Company company){
        for (int i = 0; i < company.getCategories().size(); i++){
            for (Certificate certificate: person.getCertificates()){
                if (company.getCategories().get(i).equals(certificate.getCategories())){
                    return true;
                }
            }
        }
        return false;
    }

    //Check language in person is exist
    private Boolean checkLanguage(Person person){
        if (person.getLangs().size() > 0 && !person.getLangs().isEmpty()){
            return true;
        }
        return false;
    }

    //Check Vehicle in person
    private Boolean checkVehicle(Person person){
        for (Verhicle verhicle: person.getVerhicles()){
            if (verhicle.getStatus()){
                return true;
            }
        }
        return false;
    }

    //Check status of company
    private Boolean checkCompanyStatus(Company company){
        if (company.getStatus() && !company.getCurrency().isEmpty()){
            return true;
        }
        return false;
    }

    //Check currency employee input có phù hợp với company
    private Boolean checkCurrency(EmployeeDto dto,Company company){
        for (int i = 0; i < company.getCurrency().size(); i++){
            if (company.getCurrency().get(i).equals(dto.getCurrency())){
                return true;
            }
        }
        return false;
    }

    //Check input khi insertOne employee
    public int checkInsert(EmployeeDto employeeDto){
        Company company = companyRepo.getCompanyById(employeeDto.getCompanyId());
        Person person = personRepo.getPersonById(employeeDto.getPersonId());
        //1. Check company and person does not exist
        if (company == null || person == null){
            return 0;
        }
        //2. Check thông tin nhân viên hợp lệ
        Boolean checkCategory = checkCategory(person, company);
        Boolean checkLanguage = checkLanguage(person);
        Boolean checkVehicle = checkVehicle(person);
        if (!checkCategory || !checkLanguage || !checkVehicle){
            return 2;
        }

        //3. Check company status hợp lệ
        Boolean checkCompanyStatus = checkCompanyStatus(company);
        if (!checkCompanyStatus){
            return 3;
        }

        //4. Check currency employee phù hợp với company
        Boolean checkCurrency = checkCurrency(employeeDto, company);
        if (!checkCurrency){
            return 4;
        }
        //Check insert hợp lệ
        return 1;
    }



//    2. Viết API get thông tin person đang làm cho 1 company, thông tin trả về gồm:
//    a. thông tin person
//    b. thông tin của company, gồm:
//            - tên của company theo ngôn ngữ input
//    c. Mức lương của person khi đang làm cho company, quy đổi theo tiền tệ input.
//    Chỉ support cho 2 đơn vị tiền tệ là VND + USD, default tỷ giá quy đổi là 1 USD = 25000 VND

//    step 1. Tìm kiếm các person làm trong 1 company => input cần companyId
//      - Input: companyId request to employee collection
//      - Output: List<Employee> (1) => có thể lấy list personId
//      * nếu company không có employee => return không có employee

//    step 2(a).
//      - input: (1) => request to person collection
//      - ouput: List<Person> list 

//    step 3(b).
//      - input: companyId, language => request to company collection
//      - output: companyName by language
//      * nếu không có tên theo ngôn ngữ input => return không có tên ngôn ngữ(add vào list Documet)

//    step 4(c).
//      - input: (1), currency name
//      - ouput: List<Employee> quy đổi theo tiền tệ input 

//    step 5=> return API: add output (a) + (b) + (c)
    public List<Document> getPersonInf(String companyId,String language,String currency, int pageNo,int pageSize){

        //Get List Employee and list personId

        List<Document> docs = employeeRepo.getEmployees(companyId, pageNo, pageSize); //List Employee
        //Check company exist and have employee. Tìm thông tin person trong company nhưng không có employee => return
        if (docs.isEmpty()){
            return null;
        }
        List<ObjectId> personIds = new ArrayList<>(); //List personId
        for (Document document: docs){
            personIds.add(document.getObjectId("personId"));
        }

        //a. get các person
        List<Document> documents = personRepo.geListPerson(personIds);

        //b. get company information by language
        Document doc = companyRepo.getCompanyName(companyId, language);

        //c.Mức lương của person khi đang làm cho company, quy đổi theo tiền tệ input.
            //Lấy dữ liệu "docs" từ step đầu tiên để xử lý
        List<Document> docC = new ArrayList<>();
        for (Document document: docs){
            //Convert currency to input "currency"
            docC.add(convertCurrency(document, currency));
        }

        //add các output (a), (b), (c)
        List<Document> list = new ArrayList<>();
        list.add(doc);
        list.addAll(documents);
        list.addAll(docC);

        return list;
    }

    //Convert to BigDecimalsss
	private BigDecimal convertToBigDecimal(Double input){
        //Làm tròn 3 chữ số thập phân
        int scale = 3;
        BigDecimal tempBig = new BigDecimal(input);
        tempBig = tempBig.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
        return tempBig;

    }
    //Convert currency
    private Document convertCurrency(Document docs,String currency){
        Double toVND = 25000.d;
        Double toUSD = 0.00004;
        Document doc = new Document();
        //Convert to USD
        if (!docs.get("currency").equals(currency) && currency.equals("usd") && docs.get("currency").equals("vnd")){
            BigDecimal x = convertToBigDecimal(docs.getDouble("salary") * toUSD);
            doc.put("salary", x.toString());
            doc.put("person", docs.get("fullName"));
        }
        //Convert to VND
        if (!docs.get("currency").equals(currency) && currency.equals("vnd") && docs.get("currency").equals("usd")){
            BigDecimal x = convertToBigDecimal(docs.getDouble("salary") * toVND);
            doc.put("salary", x.toString());
            doc.put("person", docs.get("fullName"));
        }
        //No convert
        if (docs.get("currency").equals(currency)){
            doc.put("salary", convertToBigDecimal(docs.getDouble("salary")).toString());
            doc.put("person", docs.get("fullName"));
        }
        return doc;
    }

    
    //Get employee convert price theo currency input
    public List<Employee> getAll(int pageSize, int pageNo) {
		List<Employee> employees = employeeRepo.getAll(pageSize, pageNo);
        if (employees.isEmpty()){
            return null;
        }
		return employees;
	}


}
