package com.exampl.mongodb.service;

import com.exampl.mongodb.entity.Employee;
import com.exampl.mongodb.entity.subEntity.TourInfo;
import com.exampl.mongodb.repository.DateOpenRepository;
import com.exampl.mongodb.repository.ExchangeRepository;
import com.exampl.mongodb.repository.TourPriceRepository;
import com.exampl.mongodb.repository.TourRepository;
import com.exampl.mongodb.entity.DateOpen;
import com.exampl.mongodb.entity.Exchange;
import com.exampl.mongodb.entity.Tour;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.print.Doc;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service
public class TourService {
    @Autowired
    private TourRepository tourRepository;
    @Autowired
    private DateOpenRepository dateOpenRepository;
    @Autowired
    private TourPriceRepository tourPriceRepository;

//    - Check input numSlot => return nếu không có tour nào mà slot phù hợp
//1. Get list thông tin tour => request to tour collection
//  - Input: language, numSlot
//  - Output: (1)
//        + infos: filter theo language input, không có lấy language đầu tiên
//        + slot: Hiển thị các tour có slot >= numSlot input
//        + list tourId(1)
//2. Get list tourId đang mở bán => request to dateOpen collection
//   - Input: date, status(true), listID(1)
//   - Output: list tourId(2)
//     * return null if openDates isEmpty => Không có tour nào mở bán vào "date"
//3. get list tourId đang mở bán và ngày mở bán nằm trong ngày áp dụng của giá tour => request to tourPrice collection
//   - Input: date, list tourId(2)
//   - Output: list tourPrice(3)
//     * return null if tourPrice isEmpty => Vì tours có mở bán nhưng không setting price
//4. Ráp totalPrice và quy đổi tiền tệ, check filter language của infos
//   - Input: output(1), currency, tourPrice(3)
//   - Output: theo yêu cầu
//      + thông tin tour
//      + Ngày khả dụng
//      + totalPrice
    public List<Document> getTourInfos(int pageNo,int pageSize,int numSlot,String date, String language,String currency){
        //1. Get tours
        List<Document> tours = tourRepository.getTours(pageSize, pageNo, language, numSlot);
        if(tours.isEmpty()) {
        	return null;
        }
        List<String> listID = new ArrayList<>();
        for (Document d : tours){
            listID.add(d.getString("_id"));
        }

        //2. Get list tourId đang mở bán => request to dateOpen collection
        List<ObjectId> list = new ArrayList<>();
        for (String s : listID){
            list.add(new ObjectId(s));
        }
        List<DateOpen> list1 = dateOpenRepository.getOpenTours(date, list);
        if (list1.isEmpty()){
                return null;
        }
        
        List<ObjectId> listTourId = new ArrayList<>();
        for (DateOpen dateOpen : list1) {
            listTourId.add(dateOpen.getTourId());
        }

        //3. get list tourId đang mở bán và ngày mở bán nằm trong ngày áp dụng của giá tour
        List<Document> tourPrice = tourPriceRepository.getTourPrices(date, listTourId, numSlot);
        //Check null => Vì tour có mở bán nhưng không setting price
        if (tourPrice.isEmpty()){
            return null;
        }
        //4. Ráp totalPrice và quy đổi tiền tệ
        Map<String, Document> mapPrice = convertTo(tourPrice, currency); //create hashMap price tour
        for (Document d : tours){
            Document pTour = mapPrice.get(d.get("_id").toString());
            if (pTour != null){
                d.put("dateOpen", date);
                d.put("slotInput", numSlot);
                d.put("totalPrice", pTour.get("totalPrice"));
            }
        }
        return tours;
    }

    private Map<String, Document> convertTo(List<Document> docs, String currency){
        Map<String, Document> map = new HashMap<>();
        Double toVND = 25000.d; //1 usd  = 25k vnđ
        Double toUSD = 0.00004; //1 vnđ = 0.00004 usd
        for (Document d : docs){
            //covert to VND
            if (!d.get("currency").equals(currency) && currency.equals("vnd") && d.get("currency").equals("usd")){
                Double z = d.getDouble("total") * toVND;
                Document doc = new Document();
                doc.put("totalPrice", convertToBigDecimal(z).toString());
                map.put(d.get("tourId").toString(), doc);
            }
            //covert to VND
            if (!d.get("currency").equals(currency) && currency.equals("usd") && d.get("currency").equals("vnd")){
                Double z = d.getDouble("total") * toUSD;
                Document doc = new Document();
                doc.put("totalPrice", convertToBigDecimal(z).toString());
                map.put(d.get("tourId").toString(), doc);
            }
            //no convert
            if (d.get("currency").equals(currency)){
                Double z = d.getDouble("total");
                Document doc = new Document();
                doc.put("totalPrice", convertToBigDecimal(z).toString());
                map.put(d.get("tourId").toString(), doc);
            }
        }
        return map;
    }

    public Boolean checkThread(String id) throws ExecutionException, InterruptedException {
        int check = tourRepository.demoMultipleThread(id);
        if (check == 2){
            return false;
        }
        return true;
    }

    public Boolean checkInput(int numSlot){
        Tour tour = this.tourRepository.findTourBySlot(numSlot);
        if (tour == null){
            return false;
        }
        return true;
    }

    //Convert to BigDecimal
    private BigDecimal convertToBigDecimal(Double input){
        //Làm tròn 3 chữ số thập phân
        int scale = 3;
        BigDecimal tempBig = new BigDecimal(input);
        tempBig = tempBig.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
        return tempBig;
    }

}
