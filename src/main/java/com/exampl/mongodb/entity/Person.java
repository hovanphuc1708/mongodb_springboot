package com.exampl.mongodb.entity;

import com.exampl.mongodb.entity.subEntity.Certificate;
import com.exampl.mongodb.entity.subEntity.EmailInf;
import com.exampl.mongodb.entity.subEntity.MoreInf;
import com.exampl.mongodb.entity.subEntity.Verhicle;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EntityScan

@Document(collection = "person")
public class Person {

    @Id
    private ObjectId id;
    private String firstName;
    private String lastName;
    private List<EmailInf> emails;
    private List<String> phone;
    private String sex;
    private int age;
    private List<MoreInf> infors;
    private List<String> langs;
    private List<Verhicle> verhicles;
    private List<Certificate> certificates;

}
