package com.exampl.mongodb.entity.subEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyName {
    private String name;
    private String lang;

}
