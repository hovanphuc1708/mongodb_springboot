package com.exampl.mongodb.entity.subEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoreInf {
    private String idCard;
    private String address;
    private Boolean status;

}
