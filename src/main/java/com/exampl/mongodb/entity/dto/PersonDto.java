package com.exampl.mongodb.entity.dto;

import com.exampl.mongodb.entity.subEntity.EmailInf;
import com.exampl.mongodb.entity.subEntity.MoreInf;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor

public class PersonDto {
    private String firstName;
    private String lastName;
    private List<EmailInf> emails;
    private List<String> phone;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birthDay;
    private String sex;
    private int age;
    private List<MoreInf> infors;
    private List<String> langs;

}
