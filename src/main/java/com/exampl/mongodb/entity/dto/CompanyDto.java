package com.exampl.mongodb.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CompanyDto {
    private String name;
    private List<String> employee;
    private int maxEmployee;
    private String address;
    private List<String> active;
    private String currOfSalary;
}
