package com.exampl.mongodb.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto {
    private String fullName;
    private Date workStart;
    private Double salary;
    private ObjectId companyId;
    private ObjectId personId;
    private String currency;
}
