package com.exampl.mongodb.entity;

import com.exampl.mongodb.entity.subEntity.CompanyName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.bson.types.ObjectId;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Document(collection = "company")
public class Company {
    @Id
    private ObjectId id;
    private List<CompanyName> names;
    private int employeeNumb;
    private String address;
    private String code;
    private List<String> categories;
    private List<String> currency;
    private Boolean status;
}
