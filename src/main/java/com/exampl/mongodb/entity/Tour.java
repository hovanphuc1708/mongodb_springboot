package com.exampl.mongodb.entity;

import com.exampl.mongodb.entity.subEntity.TourInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Document(collection = "tour")
public class Tour {
    @Id
    private ObjectId id;
    private List<TourInfo> infos;
    private int slot;
}
