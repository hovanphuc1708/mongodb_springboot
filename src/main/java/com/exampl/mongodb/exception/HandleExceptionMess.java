package com.exampl.mongodb.exception;

import org.springframework.http.HttpStatus;

import java.time.LocalDate;

public class HandleExceptionMess {
    public static final ErrorReponse okeException(String mess){
        ErrorReponse reponse = new ErrorReponse();
        reponse.setTime(LocalDate.now());
        reponse.setCode("200");
        reponse.setErrors(null);
        reponse.setSuccess(true);
        return reponse;
    }
}
